class FileNotFound(RuntimeError):
    def __init__(self, path):
	self.path = path

    def __str__(self):
        return 'FileNotFound ' + self.path


class ImproperlyConfigured(RuntimeError):
    def __init__(self, reason):
	self.reason = reason

    def __str__(self):
	return "ImproperlyConfigured " + self.reason
