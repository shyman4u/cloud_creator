import os
import subprocess

from multiprocessing.dummy import Pool
from logging_utils import log


def process_execution(command):
    process = subprocess.Popen(command, shell=True, cwd=os.getcwd(),
		stdin=subprocess.PIPE,
		stdout=subprocess.PIPE,
		stderr=subprocess.PIPE)
    output, error = process.communicate()
    returncode = process.returncode
    if returncode:
	log('ERROR', ' '.join([command, output, error]))
    else:
	log('DEBUG', ' '.join([command, output, error]))


def execute(command_list, subprocesses=1):
    pool = Pool(subprocesses)
    log('DEBUG', 'executing ' + str(command_list))
    for i in pool.imap(process_execution, command_list):
	pass
