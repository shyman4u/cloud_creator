from utils import settings, exceptions
import os
import yaml
import pprint


def extract(args):
    zone_path = os.path.join(settings.PATH_TO_YAMLS, args.name + '.yml')
    if os.path.exists(zone_path) and os.path.isfile(zone_path):
	yml_file = open(zone_path)
	zone_dict = yaml.load(yml_file)
	yml_file.close()
    else:
	raise exceptions.FileNotFound(zone_path)
    return exctract_networks(zone_dict), exctract_nodes(zone_dict, args.vm, args.exclude)


def exctract_nodes(zone_dict, vm, exclude):
    if not 'nodes' in zone_dict:
	raise exceptions.ImproperlyConfigured('no "nodes" key in yaml')
    if not 'networks' in zone_dict:
	raise exceptions.ImproperlyConfigured('no "networks" key in yaml')
    iface_to_network = {value.get('interface'
	): key for key, value in zone_dict.get('networks').items()}
    result = []
    for node in zone_dict.get('nodes', []):
	if exclude and node.get('name') in exclude.split(","):
            continue
        if vm and node.get('name') not in vm.split(","):
            continue
	result.append({
		'name': node.get('name'),
		'zone': zone_dict.get('name'),
		'networks': [iface_to_network.get(iface) for iface in node[
			'chef']['normal']['networking']['interfaces'].keys()], # list of networks
		'type': node.get('node_type', '').lower() or 'compute',
		'mac': node.get('netboot_mac')
		})
    if (vm or exclude) and not result:
        raise RuntimeError("no nodes from args found")
    return result


def exctract_networks(zone_dict):
    if not 'networks' in zone_dict:
	raise exceptions.ImproperlyConfigured('no "networks" key in yaml')
    return zone_dict.get('networks', {})
