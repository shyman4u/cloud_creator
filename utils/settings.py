import libvirt
from jinja2 import Environment, FileSystemLoader


DISK_PARAMS = {
    'compute': {
			'type_of_img': 'qcow2',
			'size': ['1G', '2G'],
			'vcpus': 2,
			'memory': '1024'
			},
    'ops': {
			'type_of_img': 'qcow2',
			'size': ['1G'],
			'vcpus': 2,
			'memory': '1024'
			},
    'storage': {
			'type_of_img': 'qcow2',
			'size': ['1G'],
			'vcpus': 2,
			'memory': '1024'
			},
    'access': {
			'type_of_img': 'qcow2',
			'size': ['1G'],
			'vcpus': 2,
			'memory': '1024'
			}
    }
IMAGE_PATH = '/home/shyman/images/' # in this directory all images will be created
PATH_TO_YAMLS = '/home/shyman/projects/deployment-data/data'
PATH_TO_LIBVIRT_XML = '/etc/libvirt/qemu'
JINJA_ENV = Environment(loader=FileSystemLoader('templates'))
CONN = libvirt.open()
