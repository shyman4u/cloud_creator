import json
import logging
import os

from logging.config import dictConfig

log_str_to_int = {
		'CRITICAL': 50,
		'ERROR': 40,
		'WARNING': 30,
		'INFO': 20,
		'DEBUG': 10
		}

color_map = {
	'CRITICAL': '\033[95m',
	'ERROR': '\033[91m',
	'WARNING': '\033[93m',
	'INFO': '\033[94m',
	'DEBUG': '\033[92m',
	'END': '\033[0m'
	}

def setup_logging(is_debug=False):
    conf = open(os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
           'logging_settings.json'))
    dictConfig(json.load(conf))
    conf.close()
    logger = logging.getLogger('cloud_creator')
    logger.setLevel((lambda a: 10 if a else 0)(is_debug))
    logger.log(10, 'logger started with settings {level}'.format(level=(
        lambda a: 10 if a else 30)(is_debug)))

def log(lvl, msg):
    logger = logging.getLogger('cloud_creator')
    logger.log(log_str_to_int.get(lvl, 0), color_map.get(lvl) + str(msg) + color_map.get('END'))
