from libvirt_utils import drive_manager, vm_manager, network_manager


def init(zone_name, ar1, ar2):
    wipeout(zone_name, ar1, ar2)
    drive_manager.bulk_create(ar2)
    network_manager.bulk_create(zone_name, ar1)
    vm_manager.bulk_create(ar2)

def wipeout(zone_name, ar1, ar2):
    stop(zone_name, ar1, ar2)
    vm_manager.bulk_delete(ar2)
    network_manager.bulk_delete(zone_name, ar1)
    drive_manager.bulk_delete(ar2)

def start(zone_name, ar1, ar2):
    vm_manager.bulk_start_domains(ar2)

def stop(zone_name, ar1, ar2):
    vm_manager.bulk_stop_domains(ar2)

def change_boot_type(zone_name, ar1, ar2):
    vm_manager.bulk_change_boot_type(ar2)
