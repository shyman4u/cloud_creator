from drive_manager import get_node_path
from network_manager import get_network_name
from utils import settings, logging_utils
import os


def node_name(node):
    return node.get('zone') + '.' + node.get('name')


def filter_defined_domains(node_list):
    logging_utils.log("DEBUG", "filtering existing node names from node list ")
    defined_domains = set([i.name() for i in settings.CONN.listAllDomains()])
    desired_names_to_delete = set(map(node_name, node_list))
    domains_found = defined_domains & desired_names_to_delete
    domains_not_found = desired_names_to_delete - defined_domains
    if domains_not_found:
	logging_utils.log("ERROR", 'You are trying to perform operation with '
			' undefined domains with following names:'
			'\n\n\n' + str(domains_not_found) + '\n\nskipping...')
    return map(settings.CONN.lookupByName, domains_found)


def render_xml(node, pxe_enabled=True):
    disk_paths = []
    for index, i in enumerate(node.get('size')):
        disk_paths.append(get_node_path(node, index))
    return settings.JINJA_ENV.get_template('domain.xml').render({
            'name': node_name(node),
            'disk_type': node.get('type_of_img'),
            'disk_paths': disk_paths,
            'memory': node.get('memory'),
            'vcpus': node.get('vcpus'),
            'pxe_enabled': pxe_enabled,
            'management': {
                'network_name': get_network_name(node.get("zone"), "management"),
                'mac': node.get('mac')},
            'networks': [get_network_name(node.get("zone"), i) for i in node.get('networks') if i != 'management']
        })


def change_boot_type(node):
    node.update(settings.DISK_PARAMS.get(node['type']))
    with open(os.path.join(settings.PATH_TO_LIBVIRT_XML, node_name(node) + '.xml'), 'w') as domain_xml:
        domain_xml.write(render_xml(node, False))
    logging_utils.log('DEBUG', 'edited libvirt domain ' + node_name(node))


def create_domain(node):
    node.update(settings.DISK_PARAMS.get(node['type']))
    settings.CONN.defineXML(render_xml(node))
    logging_utils.log('DEBUG', 'defined libvirt domain ' + node_name(node))


def bulk_create(node_list):
    logging_utils.log("INFO", "defining domains with libvirt")
    map(create_domain, node_list)
    logging_utils.log("WARNING", "all libvirt domains are defined")


def bulk_delete(node_list):
    logging_utils.log("INFO", "undefining domains with libvirt")
    map(lambda a: a.undefine(), filter_defined_domains(node_list))
    logging_utils.log("WARNING", "all libvirt domains are undefined")


def bulk_start_domains(node_list):
    logging_utils.log("INFO", "booting libvirt domains")
    map(lambda a: a.create(), filter_defined_domains(node_list))
    logging_utils.log("WARNING", "all libvirt domains are booted")


def bulk_stop_domains(node_list):
    logging_utils.log("INFO", "destroying libvirt domains")
    for domain in filter_defined_domains(node_list):
	if domain.isActive():
            domain.destroy()
    logging_utils.log("WARNING", "all libvirt domains are destroyed")

def bulk_change_boot_type(node_list):
    logging_utils.log("INFO", "changing boot type from net to hdd")
    map(change_boot_type, node_list)
    logging_utils.log("WARNING", "boot types are redefined")
