from utils import settings, commands_execution, logging_utils
import os


def get_node_path(node, index):
    return os.path.join(settings.IMAGE_PATH, node['zone'], node['name'] + str(index))


def get_directory_path(node):
    return os.path.join(settings.IMAGE_PATH, node['zone'])


def create_command(node):
    params = settings.DISK_PARAMS.get(node['type'])
    commands = []
    for index, size in enumerate(params.get('size')):
        commands.append("qemu-img create -f {type_of_img} {path} {size}".format(
            type_of_img=params['type_of_img'],
            path=get_node_path(node, index),
            size=size))
    return " && ".join(commands)


def delete_command(node):
    params = settings.DISK_PARAMS.get(node['type'])
    for index, size in enumerate(params.get('size')):
        commands = []
        node_path = get_node_path(node, index)
	if os.path.exists(node_path) and os.path.isfile(node_path):
            commands.append("rm -f " + node_path)
	return " && ".join(commands)
    else:
	return 'echo "no image at {path}", skipping'.format(path=node_path)


def bulk_create(node_list):
    logging_utils.log("INFO", 'creating images')
    # make sure that directory exists or create it
    if node_list:
	dir_path = get_directory_path(node_list[0])
	if os.path.exists(dir_path):
            if not os.path.isdir(dir_path):
		os.remove(dir_path)
		logging_utils.log("WARNING", "file removed created " + dir_path)
		os.makedirs(dir_path)
		logging_utils.log("WARNING", "directory created " + dir_path)
	else:
            os.makedirs(dir_path)
            logging_utils.log("WARNING", "directory created " + dir_path)
        commands_execution.execute(map(create_command, node_list), 10)
        logging_utils.log("WARNING", 'images created at ' + dir_path)


def bulk_delete(node_list):
    # delete directory 
    if node_list:
	dir_path = get_directory_path(node_list[0])
        logging_utils.log("INFO", 'deleting images from ' + dir_path)
        commands_execution.execute(map(delete_command, node_list), 10)
        logging_utils.log("WARNING", 'all images deleted from ' + dir_path)
        if os.path.exists(dir_path) and not os.listdir(dir_path):
            os.removedirs(dir_path)
            logging_utils.log("WARNING", 'directory removed ' + dir_path)
