from utils import settings, logging_utils
from netaddr import IPNetwork


def get_network_name(zone_name, net_name):
    name = '.'.join([zone_name, net_name])
    if len(name) > 11:
        name = name[:11]
    return name


def bulk_create(zone_name, networks_list):
    logging_utils.log("DEBUG", "creating networks")
    for k, v in networks_list.items():
        params = {'name': get_network_name(zone_name, k)}
	if k == "management":
            subnet_addresses = [str(i.network) for i in IPNetwork(v['all']['cidrs'][0]).subnet(32)]
            params.update({
			'ip': subnet_addresses[1],
			'mask': str(IPNetwork(v['all']['cidrs'][0]).netmask),
			'start': subnet_addresses[2],
			'end': subnet_addresses[-1]
		})
        settings.CONN.networkDefineXML(settings.JINJA_ENV.get_template(
                'network.xml').render(params))
        net = settings.CONN.networkLookupByName(params.get('name'))
        net.setAutostart(True)
        net.create()
	logging_utils.log("DEBUG", "created network " + params.get("name"))
    logging_utils.log("WARNING", "all networks are created")


def bulk_delete(zone_name, networks_list):
    logging_utils.log("DEBUG", "deleting all the networks")
    list_of_networks = [i.name() for i in settings.CONN.listAllNetworks()]
    for k, v in networks_list.items():
	if get_network_name(zone_name, k) in list_of_networks:
            net = settings.CONN.networkLookupByName(get_network_name(zone_name, k))
            if net.isActive():
                net.destroy()
            net.undefine()
    logging_utils.log("WARNING", "deleted all networks")
